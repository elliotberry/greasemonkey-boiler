const fetch = require('node-fetch');


const getToken = async function () {
  var myHeaders = new Headers();
  myHeaders.append(
    "Authorization",
    "Basic ektiWHBEUEhrQmRkblZqdjdCOmdiN2NDSGZwNlJ3UGJWRXY2UnF4S3pzbWtlQ3JkdThu"
  );
  myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

  var urlencoded = new URLSearchParams();
  urlencoded.append("grant_type", "client_credentials");

  var requestOptions = {
    method: "POST",
    headers: myHeaders,
    body: urlencoded,
    redirect: "follow",
  };

  fetch("https://bitbucket.org/site/oauth2/access_token", requestOptions)
    .then((response) => response.text())
    .then((result) => console.log(result))
    .catch((error) => console.log("error", error));
};
const create = async function () {
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", 'multipart/related; boundary="A100x"');
  myHeaders.append("cache-control", "no-cache");
  myHeaders.append(
    "Authorization",
    "Bearer wdNFgnipx5HMJP1gZLaxpbHZ8nvfDSty-tbsCsfwCyULCknCzIEiACagxMYCFFZ0GiX4pUc6Lv_fPeAbLfktAoJR-K_9UVER_xzeQIknsBaPy5j0nty7_LwA8Gg4WAxr_49zi51PAitOFK1bsTdT"
  );
  myHeaders.append("Content-Type", "text/plain");

  var raw =
    '--A100x\nContent-Type: application/json; charset="utf-8"\nMIME-Version: 1.0\nContent-ID: snippet\n\n{\n  "title": "My snippet",\n  "is_private": true,\n  "files": {\n      "foo.txt": {}\n    }\n}\n\n--A100x\nContent-Type: text/plain; charset="utf-8"\nMIME-Version: 1.0\nContent-Transfer-Encoding: 7bit\nContent-ID: "foo.txt"\nContent-Disposition: attachment; filename="foo.txt"\n\nfoo';

  var requestOptions = {
    method: "POST",
    headers: myHeaders,
    body: raw,
    redirect: "follow",
  };

  fetch("https://api.bitbucket.org/2.0/snippets/erberry", requestOptions)
    .then((response) => response.text())
    .then((result) => console.log(result))
    .catch((error) => console.log("error", error));
};

const update = async function (id) {
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", 'multipart/related; boundary="A100x"');
  myHeaders.append("cache-control", "no-cache");
  myHeaders.append(
    "Authorization",
    "Bearer wdNFgnipx5HMJP1gZLaxpbHZ8nvfDSty-tbsCsfwCyULCknCzIEiACagxMYCFFZ0GiX4pUc6Lv_fPeAbLfktAoJR-K_9UVER_xzeQIknsBaPy5j0nty7_LwA8Gg4WAxr_49zi51PAitOFK1bsTdT"
  );
  myHeaders.append("Content-Type", "text/plain");

  var raw =
    '--A100x\nContent-Type: application/json; charset="utf-8"\nMIME-Version: 1.0\nContent-ID: snippet\n\n{\n  "title": "My snippet",\n  "is_private": true,\n  "files": {\n      "foo.txt": {}\n    }\n}\n\n--A100x\nContent-Type: text/plain; charset="utf-8"\nMIME-Version: 1.0\nContent-Transfer-Encoding: 7bit\nContent-ID: "foo.txt"\nContent-Disposition: attachment; filename="foo.txt"\n\nfoo';

  var requestOptions = {
    method: "POST",
    headers: myHeaders,
    body: raw,
    redirect: "follow",
  };

  fetch("https://api.bitbucket.org/2.0/snippets/erberry/" + id, requestOptions)
    .then((response) => response.text())
    .then((result) => console.log(result))
    .catch((error) => console.log("error", error));
};
