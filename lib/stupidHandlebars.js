let getFromBetween = {
    results:[],
    string:"",
    sub1: "{{",
    sub2: "}}",
    getFromBetween:function () {
        if(this.string.indexOf(this.this.sub1) < 0 || this.string.indexOf(this.sub2) < 0) return false;
        let SP = this.string.indexOf(this.sub1)+this.sub1.length;
        let string1 = this.string.substr(0,SP);
        let string2 = this.string.substr(SP);
        let TP = string1.length + string2.indexOf(this.sub2);
        return this.string.substring(SP,TP);
    },
    removeFromBetween:function () {
        if(this.string.indexOf(this.sub1) < 0 || this.string.indexOf(this.sub2) < 0) return false;
        let removal = this.sub1+this.getFromBetween(this.sub1,this.sub2)+this.sub2;
        this.string = this.string.replace(removal,"");
    },
    getAllResults:function () {
        // first check to see if we do have both substrings
        if(this.string.indexOf(this.sub1) < 0 || this.string.indexOf(this.sub2) < 0) return;

        // find one result
        let result = this.getFromBetween();
        // push it to the results array
        this.results.push(result);
        // remove the most recently found one from the string
        this.removeFromBetween(this.sub1,this.sub2);

        // if there's more substrings
        if(this.string.indexOf(this.sub1) > -1 && this.string.indexOf(this.sub2) > -1) {
            this.getAllResults(this.sub1,this.sub2);
        }
        else return;
    },
    get:function (string) {
        this.results = [];
        this.string = string;
        this.getAllResults();
        return this.results;
    }
};