const gulp = require("gulp");
const plumber = require("gulp-plumber");
const fs = require('fs');
const Handlebars = require('handlebars');



async function uploadSnippet() {
  let config = await require('./config.json');
  if (config.bitbucketID.length > 0) {

  }
  else {
    
  }
}


async function buildFiles() {
    let allData = {};
    let main = await fs.readFileSync('./lib/boilerplate.js', 'utf-8');
    let template = await Handlebars.compile(main);
    allData.config = await require('./config.json');
    allData.theScript = await fs.readFileSync('./src/script.js', 'utf-8');
    let result = template(allData);
    fs.writeFileSync('./dist/script.js', result);
}



// Watch files
function watchFiles() {
  gulp.watch("./*.js", build);
  gulp.watch("./*.json", build);
}

// define complex tasks
const build = gulp.series(buildFiles);
const watch = gulp.parallel(watchFiles, buildFiles);

exports.build = build;
exports.default = build;
exports.watch = watch;
